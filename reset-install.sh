#!/usr/bin/bash

set -x

CONTAINERS=`sudo /usr/bin/docker ps -a -q`
sudo /usr/bin/docker stop $CONTAINERS
sudo docker system prune --force
rm -rf kolla-ansible
sudo rm -rf /etc/kolla
sudo rm -rf /etc/ansible
sudo rm -rf /etc/openstack
sudo docker volume rm $(sudo docker volume ls -q)
