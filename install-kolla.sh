#!/bin/bash

#https://docs.openstack.org/kolla-ansible/latest/user/quickstart.html
#git/puppet/modules/openstack/files/wmcs-kolla-ansible-evaluation.sh

# this is not idompotent, I believe the kolla-genpwd command overwrites the old passwords

source secrets.sh

set -e

# puppet will break things disable it
sudo puppet agent --disable "puppet messing up openstack testing"

sudo apt install pip python3-venv gcc python3-dev -y

mkdir -p kolla-ansible/config
cd kolla-ansible

python3 -m venv .venv/kolla
source .venv/kolla/bin/activate

pip install --upgrade pip

pip install ansible==6.7.0
pip install git+https://opendev.org/openstack/kolla-ansible@16.1.0

sudo mkdir -p /etc/kolla
sudo chown $USER:wikidev /etc/kolla

cp -r .venv/kolla/share/kolla-ansible/etc_examples/kolla/* /etc/kolla

sudo mkdir -p /etc/ansible
sudo chown "$USER":wikidev /etc/ansible
cat << EOF > /etc/ansible/ansible.cfg
[defaults]
# Better error output
stdout_callback=debug
stderr_callback=debug
EOF

cp .venv/kolla/share/kolla-ansible/ansible/inventory/all-in-one . 

kolla-ansible install-deps

kolla-genpwd

export IP=$(facter networking.ip)
export RABBIT_PW=$(grep ^rabbitmq_password: /etc/kolla/passwords.yml | awk '{print $2}')
export KEYSTONE_PW=$(grep ^keystone_database_password: /etc/kolla/passwords.yml | awk '{print $2}')
envsubst < ../keystone.conf > config/keystone.conf

cat << EOF > /etc/kolla/globals.yml
---
workaround_ansible_issue_8743: yes
kolla_base_distro: "debian"
kolla_internal_vip_address: "$(facter networking.ip)"
network_interface: "$(facter networking.primary)"
neutron_external_interface: "dummy1"
enable_haproxy: "no"
nova_compute_virt_type: "qemu"

enable_horizon: "yes"
enable_magnum: "yes"
#node_custom_config: "./config"
#keystone_admin_user: "kollaminda"
#openstack_auth:
#  auth_url: "{{ keystone_internal_url }}"
#  username: "kollaminda"
#  password: "${keystonePassword}"
#  project_name: "{{ keystone_admin_project }}"
#  domain_name: "default"
#  user_domain_name: "default"
EOF


##########
## WARNING: workaround for https://bugs.launchpad.net/kolla-ansible/+bug/1989791
if grep $(facter networking.ip) /etc/hosts | awk '{print $2}' | grep -F .
then
    old_line="$(facter networking.ip) $(facter hostname).$(facter domain) $(facter hostname)"
    new_line="$(facter networking.ip) $(facter hostname) $(facter hostname).$(facter domain)"
    sudo sed -i s/"$old_line"/"$new_line"/g /etc/hosts
fi
## END OF WARNING
##########

sudo ip link add dummy1 type dummy || true


kolla-ansible -i all-in-one bootstrap-servers && kolla-ansible -i all-in-one prechecks && kolla-ansible -i all-in-one deploy

kolla-ansible post-deploy

sudo cp /etc/kolla/clouds.yaml /etc/openstack/
sudo chmod a+r /etc/openstack/clouds.yaml
openstack --os-cloud=kolla-admin endpoint list
openstack --os-cloud=kolla-admin hypervisor list
openstack --os-cloud=kolla-admin user list
