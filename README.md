# cloud-deploy

Install:
Deploy a new bullseye VM. Copy the encryption key (cloud-deploy.key) to your home directory.

```
sudo apt install git-crypt
git clone https://gitlab.wikimedia.org/repos/cloud/cloud-vps/cloud-deploy.git
cd cloud-deploy/
git-crypt unlock ~/cloud-deploy.key
bash install-kolla.sh
```

If you want to re-install:
```
bash reset-install.sh
bash install-kolla.sh
```

Script is not quite idempotent, if you're testing on the same system that you ran the install on you should run:
```
cd kolla-ansible # should be wherever the install-kolla.sh was run
source /etc/kolla/admin-openrc.sh
source .venv/kolla/bin/activate
Then either:
kolla-ansible -i all-in-one reconfigure
```

or if it wasn't mostly installed:
```
kolla-ansible -i all-in-one bootstrap-servers && kolla-ansible -i all-in-one prechecks && kolla-ansible -i all-in-one deploy

kolla-ansible post-deploy

sudo cp /etc/kolla/clouds.yaml /etc/openstack/
sudo chmod a+r /etc/openstack/clouds.yaml
```
